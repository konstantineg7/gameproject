// Copyright Epic Games, Inc. All Rights Reserved.

#include "Evil_Is_Coming.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Evil_Is_Coming, "Evil_Is_Coming" );

DEFINE_LOG_CATEGORY(LogEvil_Is_Coming)
 