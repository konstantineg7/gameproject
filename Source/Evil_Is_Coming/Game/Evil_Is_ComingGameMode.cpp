// Copyright Epic Games, Inc. All Rights Reserved.

#include "Evil_Is_ComingGameMode.h"
#include "Evil_Is_ComingPlayerController.h"
#include "Evil_Is_Coming/Character/Evil_Is_ComingCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEvil_Is_ComingGameMode::AEvil_Is_ComingGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AEvil_Is_ComingPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
//Blueprint'/Game/Blueprint/Character/TopDownCharacter.TopDownCharacter'