// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Evil_Is_ComingGameMode.generated.h"

UCLASS(minimalapi)
class AEvil_Is_ComingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEvil_Is_ComingGameMode();
};



