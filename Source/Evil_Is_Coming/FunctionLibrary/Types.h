// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	CastWalk_State UMETA(DisplayName ="CastWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State")

};
USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	  float CastWalkSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	  float WalkSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	  float RunSpeed = 800.0f;
};

UCLASS()

class EVIL_IS_COMING_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
