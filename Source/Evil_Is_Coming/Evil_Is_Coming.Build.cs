// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Evil_Is_Coming : ModuleRules
{
	public Evil_Is_Coming(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
